<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PropertySearch;
use App\Http\Resources\PropertyCollection;
use App\Property;

class PropertyController extends Controller
{
    const SEARCH_RESULT_ITEMS_PER_PAGE = null;

    /**
     * Associative array linking query parameters with the real fields name
     * In ase of EAV implementation should be refactored
     *
     * @var array
     */
    public static $comoditiesList = [
        'bedrooms' => [
            'title' => 'Bedrooms',
            'type' => 'numeric',
            'condition' => 'equal',
            'field' => 'bedrooms'
        ],
        'bathrooms' => [
            'title' => 'Bathrooms',
            'type' => 'numeric',
            'condition' => 'equal',
            'field' => 'bathrooms'
        ],
        'storeys' => [
            'title' => 'Storeys',
            'type' => 'numeric',
            'condition' => 'equal',
            'field' => 'storeys'
        ],
        'garages' => [
            'title' => 'Garages',
            'type' => 'numeric',
            'condition' => 'equal',
            'field' => 'garages'
        ],
    ];

    /**
     * Returns array of commodities
     *
     * @return array
     */
    public static function getCommoditiesList()
    {
        return self::$comoditiesList ;
    }


    /**
     * Returns search results by frontend request
     *
     * @param PropertySearch $request
     * @return PropertyCollection
     */
    public function search(PropertySearch $request)
    {
        // Just for demo purposes you can set execution delay in  .env file
        // (to catch spinning indicator and controls lock just after the click)

        if (env('EMULATE_REQUEST_DELAY_IN_SECONDS')) {
            sleep(env('EMULATE_REQUEST_DELAY_IN_SECONDS'));
        }

        $query = $this->buildSearchQuery($request);

        if (is_null(self::SEARCH_RESULT_ITEMS_PER_PAGE)) {

            return new PropertyCollection($query->get());
        }

        // TODO pagination
        //return new PropertyCollection($query->paginate(1));



        return new PropertyCollection($query->get());
    }


    /**
     * Returns an information for search form building (ex. min/max price)
     * and commodities fields information
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearchFormData()
    {

        // get prices range for min and max price form fields
        $min = Property::query()->min('price');
        $max = Property::query()->max('price');

        // get commodities fields

        $commoditiesFields = collect(self::getCommoditiesList())->mapWithKeys(function ($item, $key) {

            return [$key => [
                'key' => $key,
                'title'        => $item['title'],
                'type'        => $item['type'],
                'condition'      => $item['condition'],
            ]];
        })->values();


        return response()->json([
            'status' => true,
            'data' => [
                'price_from' => $min,
                'price_to' => $max,
                'commoditiesFields' => $commoditiesFields
            ],

        ]);


    }


    /**
     * Builds query object based on request data
     * For the moment looks for commodities fields in the same table,
     * but as I mentioned before in case of EAV using this should be refactored
     *
     * @param $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function buildSearchQuery($request)
    {
        $query = Property::query();

        if (isset($request->name)) {
            $query->where('name', 'like', "%{$request->name}%");
        }

        if (isset($request->price_from)) {
            $query->where('price', '>=', $request->price_from);
        }
        if (isset($request->price_to)) {
            $query->where('price', '<=', $request->price_to);
        }

        $commoditiesList = self::getCommoditiesList();

        $postedCommodities = isset($request->commodities) && is_array($request->commodities) ? $request->commodities : [];

        foreach ($commoditiesList as $commodityListItemName => $commodityListItemData) {

            if (isset($postedCommodities[$commodityListItemName])) {

                switch ($commodityListItemData['condition']) {
                    case "equal":
                        $query->where($commodityListItemData['field'], $postedCommodities[$commodityListItemName]);
                        break;
                }
            }
        }

        return $query;
    }
}
