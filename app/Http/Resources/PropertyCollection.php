<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $remappedPropertiesCollection =  $this->collection->mapWithKeys(function ($item, $key) {

            return [$key => [
                'id'        => $item['id'],
                'name'      => $item['name'],
                'price'     => $item['price'],
                'bedrooms'  => $item['bedrooms'],
                'bathrooms' => $item['bathrooms'],
                'storeys'   => $item['storeys'],
                'garages'   => $item['garages'],
            ]];
        });

        return [
            'status' => true,
            'data' => $remappedPropertiesCollection,
            'meta' => [
                'page' => 'pageNumber',
            ],
        ];

    }
}
