<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Foundation\Http\FormRequest;


class PropertySearch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'max:255',
            'price_from'    => 'numeric|min:0',
            'price_to'      => 'numeric|min:0',
            'commodities.bedrooms'      => 'numeric|min:0',
            'commodities.bathrooms'     => 'numeric|min:0',
            'commodities.storeys'       => 'numeric|min:0',
            'commodities.garages'       => 'numeric|min:0',
            'page'          => 'numeric|min:1',

        ];

    }

    protected function failedValidation(Validator $validator)
    {
        $errors = collect($validator->errors())->mapWithKeys(function ($item, $key) {

            $key = preg_replace('/commodities\./', '',$key);

            return [$key => $item];
        });


        $response = [
            'status' => false,
            'data' => [],
            'errors' => $errors
        ];

        throw new HttpResponseException(response()->json($response, 422));
    }
}
