<?php

namespace App\Traits\Database;

use Illuminate\Support\Facades\DB;

/**
 * Trait SeedDataCanBeLoadedFromCsv
 *
 * Adds the ability to load the whole bunch of table data from csv file
 *
 * @package App\Traits\Database
 */
trait SeedDataCanBeLoadedFromCsv
{
    // TODO: CSV fields mapping and data import pre-validation
    // Additionally fields mapping possibility could be taken into consideration
    // (csv field name -> database table field name).
    // In this example fields name are being toggled to lower case.
    // Also for the moment one file with fixed valid data set is in use,
    // in real life the quality of the CSV tables could be unpredictable, so additional validation
    // features should be implemented in SeedDataCanBeLoadedFromCsv trait
    // or even in the new Seeder class extension.


    /**
     * Imports data from csv file
     *
     * Some parameters could be bassed via $parameters parameter or set in .env file:
     * DATABASE_SEED_CSV_IMPORT_DIR - place to hold imported csv files
     * DATABASE_SEED_CSV_IMPORT_DELIMITER - csv fields delimiter
     * DATABASE_SEED_CSV_IMPORT_ENCLOSURE - enclosure character for csv fields
     * DATABASE_SEED_CSV_IMPORT_TO_LOWER_CASE - if set will allow to use lower-cased form
     * of csv columns names to match table fields for example
     *
     * @param $table
     * @param $fileName
     * @param array $parameters
     * @return bool
     */
    public function loadFromCsv($table, $fileName, $parameters = [])
    {

        $csvDir = isset($parameters['DATABASE_SEED_CSV_IMPORT_DIR'])
            ? $parameters['DATABASE_SEED_CSV_IMPORT_DIR']
            : env('DATABASE_SEED_CSV_IMPORT_DIR',base_path() . '/database/seeds/csv/');

        $delimiter = isset($parameters['DATABASE_SEED_CSV_IMPORT_DELIMITER'])
            ? $parameters['DATABASE_SEED_CSV_IMPORT_DELIMITER']
            : env('DATABASE_SEED_CSV_IMPORT_DELIMITER',';');

        $enclosure = isset($parameters['DATABASE_SEED_CSV_IMPORT_ENCLOSURE'])
            ? $parameters['DATABASE_SEED_CSV_IMPORT_ENCLOSURE']
            : env('DATABASE_SEED_CSV_IMPORT_ENCLOSURE','"');

        $toLowerCase = isset($parameters['DATABASE_SEED_CSV_IMPORT_TO_LOWER_CASE'])
            ? $parameters['DATABASE_SEED_CSV_IMPORT_TO_LOWER_CASE']
            : false;

        if (!$file = fopen($csvDir . $fileName, 'r')) {
            return false;
        }

        $destinationFileName = $csvDir . $fileName;

        if (!$destinationFile = fopen($destinationFileName, 'r')) {
            return false;
        }

        $columns = fgetcsv($destinationFile, null, $delimiter, $enclosure);

        if ($toLowerCase) {

            $columns = $this->snakeCsvColumnsNames($columns);
        }

        while ($csvRow = fgetcsv($destinationFile, null, $delimiter, $enclosure)) {
            $row = [];
            foreach ($csvRow as $key => $value) {
                if ($value != 'NULL') {
                    $row[$columns[$key]] = $value;
                }
            }

            $created = \Carbon\Carbon::now();
            $row['created_at'] = $created;
            $row['updated_at'] = $created;

            DB::table($table)->insert($row);
        }

        fclose($destinationFile);

        return true;
    }

    /**
     * Brings columns names from imported file to lower-cased form
     * to match database table structure
     *
     * @param array $columns
     * @return array
     */
    public function snakeCsvColumnsNames(array $columns)
    {
        return array_map(function($d) {
            return snake_case($d);
        }, $columns);
    }
}
