## Installation notes

- Clone project from the repository
- Set appropriate permissions to allow access for the web services to storage/*, bootstrap folders
- Create .env file and set database parameters to access new database
- Add EMULATE_REQUEST_DELAY_IN_SECONDS=1 into the .env file if you want to emulate execution delay
- Run console commands:
``` 
 composer install
 php artisan key:generate
 composer dumpautoload -o
 php artisan migrate
 php artisan db:seed --class=PropertiesSeeder

 ```
Run PropertiesSeeder if you want to to create example data set, csv import will be executed automatically




Romankov Sergii 

+38 (050) 997-73-58

skype s_romankov

email sromankov@ukr.net