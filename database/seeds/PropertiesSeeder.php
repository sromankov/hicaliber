<?php

use Illuminate\Database\Seeder;
use \App\Traits\Database\SeedDataCanBeLoadedFromCsv;


class PropertiesSeeder extends Seeder
{
    use SeedDataCanBeLoadedFromCsv;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->truncate();

        $loaded = $this->loadFromCsv('properties','property-data.csv', [
            'DATABASE_SEED_CSV_IMPORT_DELIMITER'        => ',',
            'DATABASE_SEED_CSV_IMPORT_TO_LOWER_CASE'    => true,
        ]);

        if (!$loaded) {

            // Deal with the situation in some way...
        }
    }
}
