<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->decimal('price', 10, 2)->nullable()->default(null);
            $table->smallInteger('bedrooms')->unsigned()->nullable()->default(null);
            $table->smallInteger('bathrooms')->unsigned()->nullable()->default(null);
            $table->smallInteger('storeys')->unsigned()->nullable()->default(null);
            $table->smallInteger('garages')->unsigned()->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
